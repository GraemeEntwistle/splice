#!/bin/bash -e

echo 'in window 1, run'
echo 'daml sandbox --port 6666 .daml/dist/splice-0.0.1.dar'
echo 'in window 2, run'
echo 'daml json-api --ledger-host 127.0.0.1 --ledger-port 6666 --http-port 3434'

echo 'Make sure the npm packages are installed; run the following once:'
echo 'npm install'

echo 'Start populating the ledger now? (yes/no)'
read input
if [ "$input" == "yes" ]
then
  echo "About to start populating.."
  sleep 2
  node ./fetch-template-ids.js > template-ids.json
  node ./populatedata.js
  echo "Done"
fi

