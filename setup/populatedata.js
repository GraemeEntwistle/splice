const _ = require('lodash')
const log4js = require('log4js')
const dmledger = require('@digitalasset/daml-ledger')
const daml = dmledger.daml
const templateids = require('./template-ids.json')
const uuidv4 = require('uuid')
const Enum = require('enum')
const moment = require('moment')

log4js.configure({
  appenders: { ledger: { type: 'stdout' } },
  categories: { default: { appenders: ['ledger'], level: 'debug' } }
})
const logger = log4js.getLogger('ledger')

const LifeCycleEventStatusEnum = new Enum({
  LCE_Status_Proposed: 'LCE_Status_Proposed',
  LCE_Status_Questioned: 'LCE_Status_Questioned',
  LCE_Status_Replied: 'LCE_Status_Replied',
  LCE_Status_Accepted: 'LCE_Status_Accepted'
})

const LifeCycleEventTypeEnum = new Enum({
  LCE_Type_Event: 'LCE_Type_Event',
  LCE_Type_Evaluation: 'LCE_Type_Evaluation',
  LCE_Type_Barrier: 'LCE_Type_Barrier'
})


class Ledger {
  constructor(endpoint) {
    this.endpoint = endpoint
    this.headers = {}
    this.damlClient = null
  }

  setDamlClient(ledger, client) {
    this.damlClient = client
  }

  async connect(host, port) {
    this.host = host
    this.port = port
    logger.info(`connecting ${host}:${port}`)
    return new Promise((resolve, reject) => {
      dmledger.DamlLedgerClient.connect({host: host, port: port}, (error, client) => {
        if (error) reject(error)
        logger.debug(`templateid:`, templateids['LCE:LifeCycleEvent'])
        return resolve(client)
      })
    })
  }

  async create(entity) {
    const client = this.damlClient
    let issuer = 'ISSUER-01'
    let templateId = templateids['LCE:LifeCycleEvent']
        let request = {
          commands: {
            applicationId: 'SpliceDemoApp',
            workflowId: `Splice-${issuer}`,
            commandId: uuidv4.v4(),
            ledgerEffectiveTime: { seconds: 0, nanoseconds: 0 },
            maximumRecordTime: { seconds: 5, nanoseconds: 0 },
            party: issuer,
            list: [{
              commandType: 'create',
              templateId: templateId,
              arguments: {
                fields: entity
              }
            }]
          }
    }
    let submitted = await client.commandClient.submitAndWait(request)
  }

  listenTransactions() {
    const client = this.damlClient
    const filtersByParty = {}
    filtersByParty['ISSUER-01'] = { inclusive: { templateIds: [ templateids['LCE:LifeCycleEvent'] ] } }
    const request = {
      begin: { offsetType: 'boundary', boundary: dmledger.LedgerOffsetBoundaryValue.BEGIN  },
      filter: { filtersByParty: filtersByParty }
    }
    const txns = client.transactionClient.getTransactions(request)
    txns.on('data', response => {
      for (const txn of response.transactions) {
        logger.info('Transaction read:', txn.transactionId, txn)
      }
    })
    txns.on('error', error => {
      logger.error('Transaction error:', error)
      process.exit(-1)
    })
  }
}

async function setupRefdata(ledger, client, counter, retailclient, issuer, distributor, regulator) {
    const eventGroups = ['kick_in', 'coupon', 'early_redemption', 'autocall.barrier']
    const eventTypes = ['LCE_Type_Event', 'LCE_Type_Barrier', 'LCE_Type_Evaluation']
    const currencies = ['HKD','GBP','USD']
    const frequencies = ['Monthly','Daily','BiMonthly','Quarterly']

    for (let n = 0; n != counter; n++) {
      let symsuffix = getRandomDecimal(111111, 999999)

      let spots = _.map(new Array(3), (ele, n) => {
        return daml.decimal(getRandomDecimal(11.10, 99.90, 2))
      })
      let initspots = _.map(new Array(3), (ele, n) => {
        return daml.decimal(getRandomDecimal(11.10, 99.90, 2))
      })
      let underlyings = _.map(new Array(2), (ele, n) => {
        return daml.text(`${getRandomDecimal(1111, 9999)}.HK`)
      })
      let uuids = _.map(new Array(3), (ele, n) => {
        return daml.text(`uuid:${getRandomDecimal(11111111, 99999999)}`)
      })
      let notional = daml.decimal(getRandomDecimal(100000.0, 100500.0, 1))
      let eventId = `${getRandomDecimal(1111,9999)}${_.sample(['LB','LE','LA','PeriodPayment','Accrual','PaymentNotify','LateRedemption','Expiry','MagicCall'])}_${getRandomDecimal(11111,99999)}`

      let entity = {
        "status": daml.enum('LCE_Status_Proposed'),
        "regulators": daml.list([daml.party(regulator)]),
        "client": daml.party(retailclient),
        "trade": daml.record({"issuer": daml.party(issuer), "buyer": daml.party(distributor), "notional": notional}),
        "product": daml.record({
    "symbol": daml.text(`SAMPLE${symsuffix}`),
    "isin": daml.text(`ISIN${symsuffix}`),
          "uuids": daml.list(uuids),
          "underlyings": daml.list(underlyings),
          "spots": daml.list(spots),
          "initSpots": daml.list(initspots),
          "payoffCurrency": daml.text(_.sample(currencies)),
          "startDate": daml.date(getRandomDate(new Date(2019,12,1), new Date(2019,12,20))),
          "endDate": daml.date(getRandomDate(new Date(2021,12,1), new Date(2021,12,20))),
          "issueDate": daml.date(getRandomDate(new Date(2019,1,1), new Date(2019,2,1))),
          "frequency": daml.text(_.sample(frequencies))
        }),
        "comment": daml.record({ "commentText": daml.text(getRandomString(6)) }),
        "eventId": daml.text(eventId),
    "eventDate": daml.date(getRandomDate(new Date(), new Date(2020,4,0))),
        "eventVersion": daml.int64(getRandomDecimal(1,3)),
        "eventType": daml.enum(_.sample(eventTypes)),
        "eventGroup": daml.text(_.sample(eventGroups)),
        "content": daml.text(getRandomString(10))
      }
      await ledger.create(entity)
    }
    ledger.listenTransactions()
}

function getRandomDecimal(min, max, precision) {
  return _.random(min, max, true).toFixed(precision)
}
function getRandomString(len) {
  return _.shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ').splice(0, len).join('')
}
function getRandomDate(from, end, format='YYYY-MM-DD') {
  return moment(new Date(from.getTime() + Math.random() * (end.getTime() - from.getTime()))).toDate()
}

module.exports = {
  Ledger,
  logger 
}

async function main() {
  let [, , host, port] = process.argv
  host = host || 'localhost'
  port = port || 6666
  const ledger = new Ledger()
  try {
    let client = await ledger.connect(host, port)
    ledger.setDamlClient(ledger, client)
    const maxcount = 50
    const tries = [
      [maxcount, 'CLIENT-01', 'ISSUER-01', 'DISTRIBUTOR-01', 'REGULATOR-01'],
      [maxcount, 'CLIENT-01', 'ISSUER-01', 'DISTRIBUTOR-02', 'REGULATOR-01'],
      [maxcount, 'CLIENT-01', 'ISSUER-01', 'DISTRIBUTOR-03', 'REGULATOR-01'],
      [maxcount, 'CLIENT-02', 'ISSUER-01', 'DISTRIBUTOR-01', 'REGULATOR-01'],
      [maxcount, 'CLIENT-02', 'ISSUER-01', 'DISTRIBUTOR-02', 'REGULATOR-01'],
      [maxcount, 'CLIENT-02', 'ISSUER-01', 'DISTRIBUTOR-03', 'REGULATOR-01'],
      [maxcount, 'CLIENT-03', 'ISSUER-01', 'DISTRIBUTOR-01', 'REGULATOR-01'],
      [maxcount, 'CLIENT-03', 'ISSUER-01', 'DISTRIBUTOR-02', 'REGULATOR-01'],
      [maxcount, 'CLIENT-03', 'ISSUER-01', 'DISTRIBUTOR-03', 'REGULATOR-01'],
    ]
    _.forEach(tries, (item, n) => {
      setupRefdata(ledger, client, item[0], item[1], item[2], item[3], item[4])
    })
  } catch (e) {
    logger.error(e)
  }
}

return main()

