-- Copyright (c) 2019 Digital Asset (Switzerland) GmbH and/or its affiliates. All rights reserved.
-- SPDX-License-Identifier: Apache-2.0

daml 1.2
module Tests.Iou where

import LCE

iou_test = scenario do
    issuer1 <- getParty "ISSUER-01"
    client1 <- getParty "CLIENT-01"

  -- Generate a trade with two parties
    lcePropCid <- submit issuer1 do
        create LifeCycleEvent with
            status    = LifeCycleEventStatusEnum_Proposed
            eventType = LifeCycleEventTypeEnum_Cpn_Payment
            eventDate = "07/25/2020"
            content   = "{}"
            comment   = Comment with
                correctedContent = ""
                commentText          = ""
            trade = Trade with
                issuer = issuer1
                buyer  = client1
                isin   = "XXX0001"
                product = Product with
                    symbol = "STOMP1234"
                    underlyings = [".SPX", ".N225"]
                    payoffCurrency = "USD"
                    startDate = "07/25/2020"
                    endDate   = "07/25/2020"
                    frequency = "Monthly"
                issueDate ="07/25/2020"
                notional  = 100000.0

    --Test when client accept the proposal
    --Test when use reject the proposal
    --lceRejectCid <- submit client1 do
        --exercise lcePropCid LifeCycleEvent_Reject with rejectComment = Comment with 
                                                                    --correctedContent="EventDate"
                                                                    --commentText="2019/05/21"
    --Issuers can not Reject the proposed  
    submitMustFail issuer1 do
        exercise lcePropCid LifeCycleEvent_Reject with rejectComment = Comment with  
                                                                                 correctedContent = "EventDate"
                                                                                 commentText="fdsa"
    --Issuers can not accept the proposed
    submitMustFail issuer1 do
        exercise lcePropCid LifeCycleEvent_Accept

    --Client can not accept the reject correction
    submitMustFail client1 do  
        exercise lcePropCid LifeCycleEvent_AcceptCorrection
    
    --Client can not reject the rejection correction
    submitMustFail client1 do
        exercise lcePropCid LifeCycleEvent_RejectCorrection with rejectComment = Comment with 
                                                                                 correctedContent = "xxx"
                                                                                 commentText= "yyy"

    --Test case 1: issuer issues a event and clients accepts
    lceClientAccept <- submit client1 do 
        exercise lcePropCid LifeCycleEvent_Accept

    --Test case2: issuer issues an event and clients decline with comment error in notional
    test2 <- submit issuer1 do
        create LifeCycleEvent with
            status    = LifeCycleEventStatusEnum_Proposed
            eventType = LifeCycleEventTypeEnum_Cpn_Payment
            eventDate = "07/25/2020"
            content   = "{}"
            comment   = Comment with
                correctedContent = ""
                commentText          = ""
            trade = Trade with
                issuer = issuer1
                buyer  = client1
                isin   = "XXX0001"
                product = Product with
                    symbol = "STOMP1234"
                    underlyings = [".SPX", ".N225"]
                    payoffCurrency = "USD"
                    startDate = "07/25/2020"
                    endDate   = "07/25/2020"
                    frequency = "Monthly"
                issueDate ="07/25/2020"
                notional  = 100000.0
    
    lceClientReject <- submit client1 do 
        exercise test2 LifeCycleEvent_Reject with rejectComment = Comment with
                                                                  correctedContent = "notional"
                                                                  commentText = "Should be 12000"

    --Test Case3: Issuers issues an event and clients rejects, and Issuers accepts the correction
    --lceIssuersAcceptsClientRejectionCorrection <- submit issuer1 do
        --exercise lceClientReject LifeCycleEvent_AcceptCorrection

    --Test case3: Issues issud an event and clients rejects, and issuers rejects the correction with comment

    lceIssuersRejectsClientRejectionCorrection <- submit issuer1 do 
        exercise lceClientReject LifeCycleEvent_RejectCorrection with rejectComment = Comment with
                                                                                      correctedContent = "notional"
                                                                                      commentText = "SHUT UP, We are right"

    lceAccept <- submit client1 do
        exercise lceIssuersRejectsClientRejectionCorrection LifeCycleEvent_Accept                                                            
    return()
