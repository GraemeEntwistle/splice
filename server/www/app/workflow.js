//Global Var
var rowData = "";

/*
    Event Controllers
 */
function questionController(params){
    var date = moment().toString();
    rowData = params.node.data;
    var isin =rowData.product.isin;
    var eventType = rowData.eventType;
    document.getElementById("isinNo").textContent = isin;
    document.getElementById("eventTypeNo").textContent = _.join([eventType.split("_")[1], eventType.split("_")[2]],"_");
    $("#myModal").modal('show');

    //adding complex comments
    var comments = rowData.comment.commentText;
    if(comments != ""){
        var appendComment = "\n\n----"+ rowData.trade.issuer +" commented at: "+date+" ----\n\n";
        var result = appendComment+ comments;
        document.getElementById("correctInput").innerHTML = result;
    }
}

function replyController(params){
    var date = moment().toString();
    rowData = params.node.data;
    var isin =rowData.product.isin;
    var eventType = rowData.eventType;
    $("#isinNo").textContent = isin;
    $("#eventTypeNo").textContent = eventType.split("_")[1];
    //Add comment to the text
    var comment = rowData.comment.commentText;
    document.getElementById("correctInput").innerHTML = comment;
    //Show the modal
    $("#myModal").modal('show');
    //adding complex comments
    var comments = rowData.comment.commentText;
    var appendComment = "\n\n----"+ rowData.trade.buyer +" commented at: "+date+" ----\n\n";
    var result = appendComment + comments;
    document.getElementById("correctInput").innerHTML = result;

}

function acceptController(params){
    rowData = params.node.data;
    rowData.status = "LCE_Status_Accepted";
    var comments = rowData.comment.commentText;
    var result = "Client has accepted the trade" + comments;
    rowData.comment.commentText = result;
    DEFAULT_GRID_OPTIONS.api.refreshCells(rowData)

    //Post Controller
    postApiCall("accept", rowData.comment.commentText)
}

function onBtnClick(){
    var commentText = $("#correctInput").val();
    rowData.comment.commentText = commentText;

    if(LOGIN_PARTY.includes("DISTRIBUTOR")){
        rowData.status = "LCE_Status_Questioned";
        postApiCall("question", commentText)
    }else{
        rowData.status = "LCE_Status_Replied";
        postApiCall("reply", commentText)
    }

    DEFAULT_GRID_OPTIONS.api.refreshCells(rowData)
}

function postApiCall(status, commentText){
    console.log(commentText)
    var contractId = rowData._id;
    var postUrl = `http://${location.hostname}:${location.port}/api/v1/contract/${contractId}/`
    var client = rowData.trade.buyer;
    var issuer = rowData.trade.issuer;
    var postData = new Object();
    if(status === "accept") {
        postUrl += "LifeCycleEvent_Accept"
        postData = {} // {"choice":"accept", "contract":contractId, "party":client,"comment1":"", "comment2":commentText}
    }
    else if(status === "question"){
        postUrl += "LifeCycleEvent_Question"
        postData = { questionComment: { commentText: commentText }} //{"choice":"question", "contract":contractId, "party":client,"comment1":"", "comment2":commentText};
    } else{
        postUrl += "LifeCycleEvent_Reply"
        postData = { replyComment: { commentText: commentText } } //{"choice":"reply", "contract":contractId, "party":issuer,"comment1":"", "comment2":commentText};
    }

    //Post call
    $.ajax({
        type: "POST",
        url: postUrl,
        data: JSON.stringify(postData),
        contentType: "application/json",
        dataType: "json",
        headers:  { "X-PARTY": LOGIN_PARTY }
    })
    // $.post(postUrl, JSON.stringify(postData), function(postData, status){
    //     console.log(`${postData} and status is ${status}`);
    // })
}
