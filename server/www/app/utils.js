

function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=')

    if (sParameterName[0] === sParam)
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1])
  }
}

function getLaggard(evt, product) {
    const uuids = _.get(evt, "eventDetails.Underlyings")
    const spots = _.get(product, "spots")
    const fixings = _.get(product, "initSpots")
    const levels = _.get(evt, "eventDetails.Levels")
    if (uuids && spots && fixings && levels) {
        const dir = _.get(evt, "eventDetails.Direction") == "Up"? -1: 1
        return _.minBy(_.zip(uuids, spots, fixings, levels), x => dir * (x[1] - x[3]) / x[2])[0]
    }
}

function getLaggardDist(evt, product) {
    const spots = _.get(product, "spots")
    const fixings = _.get(product, "initSpots")
    const levels = _.get(evt, "eventDetails.Levels")
    if (spots && fixings && levels) {
        const dir = _.get(evt, "eventDetails.Direction") == "Up"? -1: 1
        const r = _.minBy(_.zip(spots, fixings, levels), x => dir *(x[0] - x[2]) / x[1])
        if (r) return dir * (r[0] - r[2]) / r[1]
    }
}

function uuidToRic(uuid, product) {
    return _.find(product.underlyings, (x, i) => _.includes(_.get(product.uuids, i), uuid))
}

function getCashPayment(pattern) {
    return params => {
        const r = _.sum(_.map(_.filter(_.get(params, "data.content.assetFlows"), x => 
            x.assetType == "Currency" && x.flowGroup && x.flowGroup.match(pattern)), "quantity"))
        if (Math.abs(r) > 1e-10) return r
    }    
}

function statusColumn(col) {
    return textColumn(_.assign({ headerName: "Status", valueGetter: "_.toUpper(_.last(_.split(data.status, '_')))", cellStyle: params => {
                const status = _.toUpper(_.last(_.split(params.data.status, '_')))
                if (status == "PROPOSED")
                    return { backgroundColor: "orange", color: "black" }
                else if (status == "QUESTIONED")
                    return { backgroundColor: "blue", color: "white" }
                else if (status == "REPLIED")
                    return { backgroundColor: "red", color: "white" }
                else{
                    return {backgroundColor:"green", color: "white"}
                }
            }}, col))
}

function numberWithCommas(x, digits) {
    return _.isNumber(x)? x.toFixed(digits || 0).replace(/\B(?=(\d{3})+(?!\d))/g, ","): x
}

function numberWithCommas(x, digits) {
    if (_.isNumber(x)) {
        var parts = x.toFixed(digits || 0).split(".")
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        return parts.join(".")
    }

    return x
}

function numberToPercent(x) {
    return `${(x * 100).toFixed(2)}%`
}

function percentFormatter() {
    return params => _.isNumber(params.value)? numberToPercent(params.value): params.value
}

function numberFormatter(digits) {
    return x => {
        return _.isNumber(x.value)? numberWithCommas(x.value, digits): x.value
    }
}

function timeFormatter(params) {
    return moment(params.value).format("HH:mm:ss")
}

function dateFormatter(params) {
    return moment(params.value == ""? moment(): params.value).format("YYYY/MM/DD")
}

function textColumn(col) {
    return Object.assign({
        headerName: col.field,
        width: 80,
        enableRowGroup: true,
        filter: "agTextColumnFilter"
    }, col)
}

function numberColumn(col, dp) {
    return Object.assign({
        headerName: col.field,
        width: 80,
        filter: "agNumberColumnFilter",
        cellStyle: { textAlign: "right" },
        valueFormatter: numberFormatter(3),
        aggFunc: "sum"
    }, col)
}

function percentageColumn(col) {
    return Object.assign({
        headerName: col.field,
        width: 80,
        filter: "agNumberColumnFilter",
        cellStyle: { textAlign: "right" },
        valueFormatter: percentFormatter(),
        aggFunc: "sum"
    }, col)
}

function dateColumn(col) {
    return Object.assign({
        headerName: col.field,
        width: 75,
        enableRowGroup: true,
        valueFormatter: dateFormatter,
        filter: "agDateColumnFilter",
        filterParams:{
            comparator: function (filterLocalDateAtMidnight, cellValue) {
                var dateAsString = cellValue;

                //var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));
                var cellDate = moment(dateAsString);
                var compareDate = moment(filterLocalDateAtMidnight).format("MM/DD/YYYY");
                if (moment(dateAsString).isSame(compareDate)) {
                    return 0;
                }
                ;

                if (moment(dateAsString).isBefore(compareDate)) {
                    return -1;
                }
                ;

                if (moment(dateAsString).isAfter(compareDate)) {
                    return 1;
                }
                ;
            }
        }
    }, col)
}
