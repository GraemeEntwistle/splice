const VIEWS = {}
VIEWS["DISTRIBUTOR-01"] = new ViewSBI("DISTRIBUTOR-01", true)
VIEWS["DISTRIBUTOR-02"] = new ViewUOB("DISTRIBUTOR-02")
VIEWS["DISTRIBUTOR-03"] = new ViewIPS("DISTRIBUTOR-03")
VIEWS["DISTRIBUTOR-04"] = new ViewSCB("DISTRIBUTOR-04")

VIEWS["CLIENT-01"] = new ViewSBI("DISTRIBUTOR-01", true)
VIEWS["CLIENT-02"] = new ViewUOB("DISTRIBUTOR-02")
VIEWS["CLIENT-03"] = new ViewIPS("DISTRIBUTOR-03")
VIEWS["CLIENT-04"] = new ViewSCB("DISTRIBUTOR-04")

VIEWS["ISSUER-01"] = new ViewUBS("ISSUER-01")
VIEWS["REGULATOR-01"] = new ViewRegulator("REGULATOR-01")
const faye = new Faye.Client(`${window.location.origin}/splice`)

faye.subscribe("/contract", message => {
  const contract = _.find(CURRENT_DATA, x => x.eventId == message.eventId)
  
  if (contract) {
    const update = [Object.assign(contract, message)]
    GRID_OPTIONS.api.batchUpdateRowData({ update: update })
  }
})

const FILTERS = {
  party: null,
  report: null,
  dateFrom: null,
  dateTo: null,
  isin: null
}

let LOGIN_PARTY  = null
let CURRENT_DATA = null
let GRID_OPTIONS = null

const DEFAULT_GRID_OPTIONS = {
  floatingFilter: true,
  animateRows: true,
  groupDefaultExpanded: 1,
  enableRangeSelection: true,
  // getContextMenuItems: getContextMenuItems,
  allowContextMenuWithControlKey: true,
  headerHeight: 16,
  rowHeight: 16,
  defaultColDef: {
    sortable: true,
    filter: true,
    resizable: true
  },
  onRowDoubleClicked: x => console.log(x.data),
  autoGroupColumnDef: {
    headerName: '',
    pinned: "left",
    cellStyle: { textAlign: "left" },
    width: 130,
    cellRendererParams: {
        suppressCount: false
    },
    filter: "agTextColumnFilter"
  },
  // columnDefs: null,
  sideBar: {
    toolPanels: [
        {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
        }]
  }
}

async function on_report_change(party, report, skiprefresh) {
  FILTERS.party = party
  FILTERS.report = report
  
  init_date_range(VIEWS[party], report)
  $("#isin").val("")

  if (!skiprefresh)
    refresh_grid()
}

async function on_date_range_change(start, end, skiprefresh) {
  FILTERS.dateFrom = start
  FILTERS.dateTo = end

  if (!skiprefresh)
    refresh_data()
}

async function on_isin_changed(skiprefresh) {
  FILTERS.isin = $("#isin").val()

  if (!skiprefresh)
    refresh_data()
}

async function get_data(party) {
  return await $.ajax({
    type:     "GET",
    url:      `http://${location.hostname}:${location.port}/api/v1/contract`,
    headers:  { "X-PARTY": party }
  })
}

function init_date_range(view, report) {
  const [start, end] = view.getDateRange(report)

  $('#daterange').daterangepicker({
    startDate: start,
    endDate: end
  }, on_date_range_change)

  on_date_range_change(start, end, true)
}

function init_view_select_client(view, pickfirst, sep) {
  console.log(view, view.availViews)
  const availViews = view.availableViews()
  const firstView  = _.head(availViews)
  if (pickfirst) {
    $("#eventtype").attr("data-selected", _.get(firstView, "id"))
    $("#eventtype").text(_.get(firstView, "name"))
    on_report_change(view.party, _.get(firstView, "id"), true)
  }

  if (sep)
    $("#eventtypelist").append($(`<div style="padding-left:10px;font-weight:bold">${!pickfirst || view.for == null? view.name(): view.alias().toUpperCase()}</div>`))
  

  _.forEach(availViews, v => {
    $("#eventtypelist").append($(`<a class="dropdown-item" onclick="update_report(this, '${view.party}', '${v.id}')">${v.name}</a>`))
  })
}

function update_report(e, party, reportId) {
  $("#eventtype").text($(e).text()).attr("data-selected", reportId)
  on_report_change(party, reportId, false)
}

function init_view_select(view) {
  $("#eventtypelist").empty()

  if (view.isClient()) {
    init_view_select_client(view, true, false)
  }
  else {
    _.forEach(view.availableClients(), (client, i) => {
      const v = VIEWS[client]
      if (v.isSelected())
        init_view_select_client(v, true, true)
    })

    _.forEach(view.availableClients(), (client, i) => {
      const v = VIEWS[client]
      if (!v.isSelected())
        init_view_select_client(v, false, true)
    })
  }
}

function filter_data(data) {
  console.log(FILTERS)
  const view = VIEWS[FILTERS.party]
  return _.filter(data, x => {
    console.log(x)
    if (_.size(x.product.underlyings) == 0) return false
      
    if (FILTERS.party)
      if (x.trade.buyer != FILTERS.party) return false

    if (FILTERS.dateFrom && FILTERS.dateTo) {
      const d = moment(x.eventDate)
      if (d < FILTERS.dateFrom || d > FILTERS.dateTo)
        return false
    }

    if (_.size(FILTERS.isin) > 0)
      if (!x.product.isin.includes(FILTERS.isin)) return false

    return view.filter(FILTERS.report, x)
  })
}

function refresh_data() {
  get_data(LOGIN_PARTY).then(data => {
    data = filter_data(data)
    _.forEach(data, x => x.content = JSON.parse(x.content))
    console.log(data)
    CURRENT_DATA = data
    GRID_OPTIONS.api.setRowData(data)
  })
}

function refresh_grid() {
  if (GRID_OPTIONS) GRID_OPTIONS.api.destroy()

  const view = VIEWS[FILTERS.party]
  const container = document.querySelector("#list")

  GRID_OPTIONS = view.gridOptions(FILTERS.report, LOGIN_PARTY)
  new agGrid.Grid(container, GRID_OPTIONS)

  refresh_data()
}

$(function() {
  $('body').bootstrapMaterialDesign()
  $("#logoff").click(function() { window.location = "/" })

  LOGIN_PARTY = getUrlParameter("party")
  
  if (LOGIN_PARTY == null) {
    $("#btnLogin").click();
    $("#goLogin").click(function() {
      window.location = "?party=" + $("#parties").val()
    })
    $(".navbar").hide()
  } else {
    $(".navbar").show()
    const view = VIEWS[LOGIN_PARTY]

    if (view) {
      view.init()

      init_view_select(view)

      refresh_grid()
    }

  }
})
