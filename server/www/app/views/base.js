function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

class BaseClientView {
  constructor(party, isDefault) {
    this.party = party
    this.isDefault = isDefault
    this.for = getUrlParameter("for")
  }

  isSelected() {
    if (this.for == null)
      return this.isDefault
    
    return this.for == this.alias()
  }

  name() { return this.party }
  
  isClient() { return true }
  
  availableViews() {
    return [{ id: "PAYMENT", name: "Payment Report"},
            { id: "BARRIER", name: "Barrier Report"},
            { id: "EVENT", name: "Event Report"}]
  }

  filter(id, record) {
    switch (id) {
      case "BARRIER":
        console.log()
        return record.eventType == "LCE_Type_Barrier" // && ["KICK_IN"].includes(record.eventGroup)
      case "PAYMENT":
        return record.eventType == "LCE_Type_Evaluation"
      case "EVENT":
        return record.eventType == "LCE_Type_Event"
    }
  }

  gridOptions(id, party) {
    return _.assign(DEFAULT_GRID_OPTIONS, { 
      columnDefs: this.columnDefs(id),
      getContextMenuItems: params => this.getContextMenuItems(params, id, party)})
  }

  getContextMenuItems(params, report, party) {
    const menu = []
    const row = params.node.data

    if (party.includes("ISSUER")) {
      switch (row.status) {
        case "LCE_Status_Proposed":
          break
        case "LCE_Status_Questioned":
          menu.push({ name:'Reply', action: () => replyController(params) })
          break
        case "LCE_Status_Replied":
          break
        case "LCE_Status_Accepted":
          break
      }
    }
    else if (party.includes("DISTRIBUTOR")) {
      switch (row.status) {
        case "LCE_Status_Proposed":
          menu.push({ name:'Question', action: () => questionController(params) })
          break
        case "LCE_Status_Replied":
          menu.push({ name:'Question', action: () => questionController(params) })
          menu.push({ name:'Accept', action: () => acceptController(params) })
          break
      }
    }

    menu.push({
        name: 'Export',
        subMenu:[
          {
            name: "CSV Export",
            action: () => GRID_OPTIONS.api.exportDataAsCsv()
          },
          {
            name: "Excel Export",
            action: () => GRID_OPTIONS.api.exportDataAsExcel()
          }
        ]
      })

    return menu
  }

  getDateRange(id) {
    switch (id) {
      case "BARRIER":
        return [moment().add(-30, 'day'), moment().add(1, 'day')]
      case "PAYMENT":
        return [moment().add(-30, 'day'), moment().add(1, 'day')]
      case "EVENT":
        return [moment(), moment().add(30, 'day')]
    }
    
  }

}
