class ViewUBS {
  constructor(party) {
    this.party = party
  }

  init() {
    $("#logo").attr("src", "img/logo.png")
    $("#list").removeClass("client").addClass("issuer")
  }

  name() { return "SBU" }
  
  isClient() { return false }

  availableClients() {
    return ["CLIENT-01", "CLIENT-02", "CLIENT-03", "CLIENT-04"]
  }
}
