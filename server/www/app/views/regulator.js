class ViewRegulator {
  constructor(party) {
    this.party = party
  }

  init() {
    $("#logo").attr("src", "img/logo-reg.png")
    $("#list").removeClass("client").addClass("regulator")
  }

  name() { return "REGULATOR-01" }
  
  isClient() { return false }

  availableClients() {
    return ["CLIENT-01", "CLIENT-02", "CLIENT-03", "CLIENT-04"]
  }
}