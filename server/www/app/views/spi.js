class ViewIPS extends BaseClientView {
  init() {
    if (this.for == "spi")
        $("#logo").attr("src", "img/logo.png")
    else
        $("#logo").attr("src", "img/client2.png")

    $("#list").removeClass("issuer").addClass("client")
  }

  alias() { return "spi" }

  columnDefs(id) {
    if (id == "BARRIER") {
      return [
        dateColumn({ headerName: "Event Date", sort: "asc", field:'date', valueGetter: "data.eventDate"}),
        statusColumn({}),
        textColumn({ headerName: "Type", valueGetter: "data.eventGroup", width: 120, filter: true } ),
        textColumn({ headerName: "Symbol", valueGetter: "data.product.symbol", width: 100 } ),
        textColumn({ headerName: "ISIN", valueGetter: "data.product.isin", width: 100 } ),
        textColumn({ headerName: "Issuer", valueGetter: "'SBU GA London'", width: 100}),
        // textColumn({ headerName: "Event Type", field: "eventType",valueGetter: x => _.join([x.data.eventType.split('_')[1],x.data.eventType.split('_')[2]],"_"), width:100}),
        // textColumn({ headerName: "Issuer", valueGetter:"data.trade.issuer"}),
        // textColumn({ headerName: "Buyer", valueGetter: "data.trade.buyer"}),
        dateColumn({ headerName: "Issue Date", valueGetter: "data.product.issueDate"}),
        dateColumn({ headerName: "Mat Date", valueGetter:"data.product.endDate"}),
        textColumn({ headerName: "Ccy", valueGetter: "data.product.payoffCurrency", width: 50}),
        numberColumn({ headerName: "Notional", valueGetter:"data.trade.notional", width: 100}),
        textColumn({ headerName: "Underlyings", width: 160, valueGetter: x => _.join(x.data.product.underlyings, " \\ ")}),
        // dateColumn({ headerName: "Trade Date", valueGetter: "data.product.startDate"}),
        textColumn({ headerName: "Laggard", valueGetter: x=> uuidToRic(_.get(x, "data.content.underlying"), x.data.product)}),
        numberColumn({ headerName: "Price(Closeing)", valueGetter:"data.content.closingPrice", width: 100}),
        numberColumn({ headerName: "Knock in Strike", valueGetter:"data.content.barrierLevel", width: 100}),
        // textColumn({ headerName: "Frequency", valueGetter:"data.product.frequency"}),

        textColumn({ headerName: "Comment", width: 150, valueGetter:"data.comment.commentText"})
      ]
    } else if (id == "PAYMENT") {
      return [
        dateColumn({ headerName: "Event Date", sort: "asc", field:'date', valueGetter: "data.eventDate"}),
        statusColumn({}),
        textColumn({ headerName: "Symbol", valueGetter: "data.product.symbol", width: 100 } ),
        textColumn({ headerName: "ISIN", valueGetter: "data.product.isin", width: 100 } ),
        textColumn({ headerName: "Issuer", valueGetter: "'SBU GA London'", width: 100}),
        textColumn({ headerName: "Payment Type", valueGetter: "data.eventGroup", width: 100, filter: true }),
        // textColumn({ headerName: "Issuer", valueGetter:"data.trade.issuer"}),
        // textColumn({ headerName: "Buyer", valueGetter: "data.trade.buyer"}),
        dateColumn({ headerName: "Issue Date", valueGetter: "data.product.issueDate"}),
        dateColumn({ headerName: "Mat Date", valueGetter:"data.product.endDate"}),
        textColumn({ headerName: "Underlyings", width: 160, valueGetter: x => _.join(x.data.product.underlyings, " \\ ")}),
        // dateColumn({ headerName: "Trade Date", valueGetter: "data.product.startDate"}),
        textColumn({ headerName: "Ccy", valueGetter: "data.product.payoffCurrency", width: 50}),
        numberColumn({ headerName: "Notional", valueGetter:"data.trade.notional", width: 100}),
        numberColumn({ headerName: "Cpn Payment", valueGetter: getCashPayment(/Coupon/), width: 100}),
        numberColumn({ headerName: "Early Redemption Payment", valueGetter: getCashPayment(/EarlyRedemption/), width: 100}),
        numberColumn({ headerName: "Expiry Payment", valueGetter: getCashPayment(/ExpiryPayment/), width: 100}),
        numberColumn({ headerName: "Notional", valueGetter:"data.trade.notional", width: 100}),
        
        // textColumn({ headerName: "Frequency", valueGetter:"data.product.frequency"}),

        textColumn({ headerName: "Comment", width: 150, valueGetter:"data.comment.commentText"})
      ]
    } else if (id == "EVENT") {
      return [
        dateColumn({ headerName: "Event Date", sort: "asc", field:'date', valueGetter: "data.eventDate"}),
        statusColumn({}),
        textColumn({ headerName: "Symbol", valueGetter: "data.product.symbol", width: 100 } ),
        textColumn({ headerName: "ISIN", valueGetter: "data.product.isin", width: 100 } ),
        textColumn({ headerName: "Issuer", valueGetter: "'SBU GA London'", width: 100}),
        textColumn({ headerName: "Event Type", valueGetter: "data.eventGroup", width: 120, filter: true }),
        dateColumn({ headerName: "Issue Date", valueGetter: "data.product.issueDate"}),
        dateColumn({ headerName: "Mat Date", valueGetter:"data.product.endDate"}),
        textColumn({ headerName: "Ccy", valueGetter: "data.product.payoffCurrency", width: 50}),
        numberColumn({ headerName: "Notional", valueGetter:"data.trade.notional", width: 100}),
        textColumn({ headerName: "Underlyings", width: 160, valueGetter: x => _.join(x.data.product.underlyings, " \\ ")}),
        textColumn({ headerName: "Laggard", valueGetter: x=> uuidToRic(getLaggard(x.data.content, x.data.product), x.data.product)}),
        percentageColumn({ headerName: "Distance", valueGetter: x => getLaggardDist(x.data.content, x.data.product), width: 60}),
        textColumn({ headerName: "Spot Level", width: 120, valueGetter: x => _.join(x.data.content.eventDetails.LastFixingLevels, " \\ ")}),
        textColumn({ headerName: "Levels", width: 120, valueGetter: x => _.join(x.data.content.eventDetails.Levels, " \\ ")}),
        
        
        textColumn({ headerName: "Comment", width: 150, valueGetter:"data.comment.commentText"})
      ]
    }
  }
}
