const os      = require('os'),
      request = require('request-promise-native'),
      moment  = require('moment'),
      _       = require("lodash"),
      tough   = require('tough-cookie'),
      log4js  = require('log4js')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

const HTTP_PROXY  = process.platform === "win32"? "http://127.0.0.1:8888": null

log4js.configure({
  appenders: { 'out': { type: 'stdout' } },
  categories: { default: { appenders: ['out'], level: 'debug' } }
})

const M  = {
  logger: log4js.getLogger(),

  websso: async (username, password, isuat) => {
    let rcas_url = ""
    let test_url = ""

    const jar = request.jar()
    let res = await request.get({ url: rcas_url, jar: jar, proxy: HTTP_PROXY, auth: { user: username, pass: password, sendImmediately: false}, ssl: false })
    res = await request.get({ url: test_url, jar: jar, proxy: HTTP_PROXY })
    return jar
  },

  getJson: async (url, auth) => {
    const res = await request.get({ url: url, jar: auth, proxy: HTTP_PROXY })
    return JSON.parse(res)
  },

  postJson: async (url, data, headers, auth) => {
    return JSON.parse(await request.post({ url: url, body: JSON.stringify(data), jar: auth, headers: _.assign({
      "content-type": "application/json"
    }, headers), proxy: HTTP_PROXY }))
  },

  post: async (url, data, auth) => {
    return await request.post({ url: url, form: data, jar: auth, headers: {
      "content-type": "text/plain"
    }, proxy: HTTP_PROXY })
  },
}

module.exports = M
