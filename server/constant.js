
module.exports = {
 TEMPLATE_QUERY: 
  `query TemplatesQuery($filter: [FilterCriterion!], $search: String!, $count: Int!, $sort: [SortCriterion!]) {
    templates(search: $search, filter: $filter, count: $count, sort: $sort) {
      totalCount
      edges {
        node {
          __typename
          id
          ... on Template {
            topLevelDecl
            contracts {
              totalCount
              __typename
            }
            __typename
          }
        }
        __typename
      }
      __typename
    }
  }`,

  CONTRACT_QUERY:
    `query ContractsQuery($filter: [FilterCriterion!], $search: String!, $includeArchived: Boolean!, $count: Int!, $sort: [SortCriterion!]) {
      contracts(filter: $filter, search: $search, includeArchived: $includeArchived, count: $count, sort: $sort) {
        totalCount
        edges {
          node {
            __typename
            id
            ... on Contract {
              createEvent {
                id
                transaction {
                  effectiveAt
                  __typename
                }
                __typename
              }
              archiveEvent {
                id
                __typename
              }
              argument
              template {
                id
                choices {
                  name
                  __typename
                }
                __typename
              }
              __typename
            }
          }
          __typename
        }
        __typename
      }
    }`,

  TEMPLATE_INSTANCE:
    `query TemplateInstance($templateId: ID!) {
      node(typename: "Template", id: $templateId) {
        ... on Template {
          id
          parameter
          topLevelDecl
          __typename
        }
        __typename
      }
    }`,

  PARAMETER_FORM_TYPE_QUERY:
    `query ParameterFormTypeQuery($id: ID!) {
      node(typename: "DamlLfDefDataType", id: $id) {
        ... on DamlLfDefDataType {
          dataType
          typeVars
          __typename
        }
        __typename
      }
    }`,

  CREATE_CONTRACT: 
    `mutation CreateContract($templateId: ID!, $argument: DamlLfValue) {
      create(templateId: $templateId, argument: $argument)
    }`,

  COMMAND_RESULTS_QUERY:
  `query CommandResultsQuery($commandIds: [ID!]!) {
    nodes(typename: "Command", ids: $commandIds) {
      __typename
      id
      ... on Command {
        status {
          __typename
          ... on CommandStatusError {
            code
            details
            __typename
          }
        }
        __typename
      }
    }
  }`,
  
  CONTRACT_EXEC: 
    `mutation ContractExercise($contractId: ID!, $choiceId: ID!, $argument: DamlLfValue) {
      exercise(contractId: $contractId, choiceId: $choiceId, argument: $argument)
    }`,

  CONTRACT_DETAILS: 
    `query ContractDetailsById($id: ID!) {
      node(id: $id, typename: "Contract") {
        ... on Contract {
          id
          argument
          archiveEvent {
            id
            __typename
          }
          agreementText
          signatories
          observers
          key
          template {
            id
            topLevelDecl
            choices {
              name
              parameter
              __typename
            }
            __typename
          }
          __typename
        }
        __typename
      }
    }`
}