module.exports = {
  SERVER_PORT:             7676,
  DEFAULT_LEDGER_ENDPOINT: "http://localhost:3434",
  DEFAULT_LOGIN_PARTY:     "ISSUER-01"
}
