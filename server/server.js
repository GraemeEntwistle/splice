const os      = require('os')
const constant = require('./constant')
const deflate = require('permessage-deflate')
const utils   = require('./utils')
const config  = require('./config')
const Ledger  = require('./ledger')
const _       = require("lodash")
const moment  = require("moment")
const express = require('express')
const http    = require('http')
const app     = express()
const PORT    = process.env.PORT || config.SERVER_PORT || 7778
const bodyParser = require('body-parser')

const faye    = require('faye')
const pubsub  = new faye.Client(`http://${os.hostname()}:${PORT}/splice`)
const bayeux  = new faye.NodeAdapter({ mount: "/splice", timeout: 45 })

app.use(bodyParser.json())
app.use(function(req, res, next) {
  const endpoint = process.env.default_ledger_endpoint || req.header("X-LEDGER") || config.DEFAULT_LEDGER_ENDPOINT
  req.party  = req.header("X-PARTY") || config.DEFAULT_LOGIN_PARTY
  console.log(`Connecting to ledger port: [${endpoint}]`)
  req.ledger = new Ledger(endpoint)
  req.ledger.login(req.party).then(next)
})

function filterData(data, query) {
  return data
}

app.post("/api/v1/contract/:id/:choice", async (req, res) => {
  utils.logger.info(`/api/v1/contract/${req.params.id}/${req.params.choice}`)
  req.ledger.getContractById("#" + req.params.id).then(contract => {
    if (contract) {
      // console.log(req.body)
      contract.exec(req.params.choice, req.body).then(data => {
        res.send(data)
        req.ledger.query("CommandResultsQuery", constant.COMMAND_RESULTS_QUERY, 
          {commandIds: [data.data.exercise]}).then(result => {
            // console.log(JSON.stringify(result))

            const oldContract = contract.toJson()
            setTimeout(function() {
              req.ledger.contracts().then(contracts => {
                const newContract = _.find(contracts, x => x.eventId == oldContract.eventId)
                if (newContract)
                  pubsub.publish("/contract", newContract)
              })
            }, 500)
          })
      })
    }
  })
})

app.get("/api/v1/contract/count", async (req, res) => {
  utils.logger.info(`/api/v1/contract/count`)
  req.ledger.contracts().then(data => {
    res.send(_.toString(_.size(filterData(data, req.query))))
  })
})

app.get("/api/v1/contract/:id", async (req, res) => {
  utils.logger.info(`/api/v1/contract/${req.params.id}`)
  req.ledger.getContractById("#" + req.params.id).then(contract => {
    if (contract) res.send(contract.toJson())
  })
})

app.get("/api/v1/contract", async (req, res) => {
  req.ledger.contracts().then(data => {
    res.send(filterData(data, req.query))
  })
})


app.use('/', express.static(__dirname + '/www'))

const server  = http.createServer(app)
bayeux.addWebsocketExtension(deflate)
bayeux.attach(server)
server.listen(PORT)


utils.logger.info(`Server started successfully on port ${PORT}`)
