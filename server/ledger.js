const utils    = require('./utils')
const constant = require('./constant')
const _        = require("lodash")
const request  = require('request-promise-native')

function TYPEID(obj) {
  return `${obj.module}:${obj.name}@${obj.package}`
}

async function _getValue(ledger, id, val) {
  const dt = _.clone(await ledger.dataType(TYPEID(id)))

  switch (dt.type) {
    case "enum":
      dt.constructor = val // TODO: validate enum value
      delete dt["constructors"]
      break
    case "record":
      dt.fields = await Promise.all(_.map(dt.fields, field => _getField(ledger, field, _.get(val, field.name))))
      break
  }

  dt.id = id

  return dt
}

async function _getFieldValue(ledger, meta, val) {
  switch (meta.type) {
    case "typecon":
      return await _getValue(ledger, meta.name, val)
    case "primitive":
      if (meta.name == "list") {
        return {
          type: meta.name,
          value: await Promise.all(_.map(val, x => _getFieldValue(ledger, meta.args[0], x)))
        }
      }
      else {
        return {
          type: meta.name,
          value: _.toString(val)
        }
      }
  }
}

async function _getField(ledger, field, val) {
  return {
    label: field.name,
    value: await _getFieldValue(ledger, field.value, val)
  }
}

function _toJson(val) {
  switch (val.type) {
    case "record":
      const r = {}
      _.forEach(val.fields, field => {
        r[field.label] = _toJson(field.value)
      })
      return r
    case "enum":
      return val.constructor
    case "decimal":
      return parseFloat(val.value)
    case "list":
      return _.map(val.value, _toJson)
    default:
      return val.value
  }
}

class Contract {
  constructor(ledger, details) {
    this.ledger  = ledger
    this.details = details
  }

  async exec(choiceName, params) {
    const choice = _.find(this.details.data.node.template.choices, x => x.name == choiceName)
    if (choice) {
      console.log(params)
      return await this.ledger.query("ContractExercise", constant.CONTRACT_EXEC, {
        argument: await _getValue(this.ledger, choice.parameter.name, params),
        choiceId: choiceName,
        contractId: this.details.data.node.id
      })
    }
  }

  toJson() {
    return _toJson(this.details.data.node.argument)
  }
}

class Template {
  constructor(ledger, id) {
    const tokens = id.split(/[:@]/)
    
    this.module  = tokens[0]
    this.name    = tokens[1]
    this.package = tokens[2]
    this.ledger  = ledger
  }

  get fullId() {
    return {
      module:  this.module,
      name:    this.name,
      package: this.package
    }
  }

  get id() {
    return TYPEID(this)
  }

  async create(obj) {
    const params = {
      argument: await _getValue(this.ledger, this.fullId, obj),
      templateId: this.id
    }

    this.ledger.createContract(params)
  }
}

class Ledger {
  constructor(endpoint) {
    this.endpoint   = endpoint
    this.paramCache = {}
  }

  async login(userId) {
    const jar = request.jar()
    const res = await await utils.postJson(`${this.endpoint}/api/session/`, { userId: userId }, {}, jar)
    this.jar = jar
  }

  async query(op, querydata, vars) {
    try {
      console.log(`ledger.query ${op}`)
      const res = await utils.postJson(`${this.endpoint}/api/graphql`, {
        operationName: op,
        query: querydata,
        variables: vars
      }, {}, this.jar)

      return res
    }
    catch (ex) {
      return null
    }
  }

  async templates(params) {
    const templates = await this.templatesQuery(params)
    return _.map(templates.data.templates.edges, x => new Template(this, x.node.id))
  }

  async contracts(params) {
    const contracts = await this.contractsQuery()
    let data = _.map(_.get(contracts, "data.contracts.edges"), c => _.assign(_toJson(c.node.argument), { _id: _.replace(c.node.id, "#", "") }))

    return data
  }

  async contracts_good(params) {
    const contracts = await this.contractsQuery()
    let data = _.map(_.get(contracts, "data.contracts.edges"), c => _.assign(_toJson(c.node.argument), { _id: _.replace(c.node.id, "#", "") }))

    console.log(`${typeof(data)}: ${data.length}`, data[data.length-1])

    let refs = {}, unqGroup = {}, unqType = {}
    _.forEach(data, d => {
      unqGroup[d.eventGroup] = _.get(unqGroup, d.eventGroup, 0) + 1
      unqType[d.eventType] = _.get(unqType, d.eventType, 0) + 1
      let key = d.eventType + '-' + d.eventGroup
      if (!_.has(refs, key)) {
        _.set(refs, key, d)
        console.log(`${d.eventType}: ${d.eventGroup}:`)
      }
    })

    console.log('-----------------------------------------------------------')
    // console.log(JSON.stringify(refs, null, 2))
    console.log('-----------------------------------------------------------')
    console.log(`unqGroup:`, JSON.stringify(unqGroup, null, 2))
    console.log(`unqType:`, JSON.stringify(unqType, null, 2))
    return data
  }

  async templatesQuery(params) {
    return (await this.query("TemplatesQuery", constant.TEMPLATE_QUERY, _.assign({
      count: 100,
      filter: [],
      search: "",
      sort: []
    }, params)))
  }

  async contractsQuery(params) {
    return await this.query("ContractsQuery", constant.CONTRACT_QUERY, _.assign({
      count: 999999,
      filter: [],
      includeArchived: false,
      search: "",
      sort: []
    }, params))
  }

  async dataType(id) {
    return (await this.parameterQuery(id)).data.node.dataType
  }

  async templateInstance(id) {
    return await this.query("TemplateInstance", constant.TEMPLATE_INSTANCE, {templateId: id})
  }

  async parameterQuery(id) {
    if (this.paramCache[id] == null)
      this.paramCache[id] = (await this.query("ParameterFormTypeQuery", constant.PARAMETER_FORM_TYPE_QUERY, {id: id}))
    
    return this.paramCache[id]
  }

  async createContract(params) {
    return await this.query("CreateContract", constant.CREATE_CONTRACT, params)
  }

  async getTemplateByName(name) {
    return _.find(await this.templates(), x => x.name == name)
  }

  async getContractById(contractId) {
    return new Contract(this, await this.query("ContractDetailsById", constant.CONTRACT_DETAILS, { id: contractId }))
  }
}


module.exports = Ledger

async function test() {
  const ledger = new Ledger("http://localhost:7500")
  await ledger.login("ISSUER-01")
  console.log(await ledger.contracts())
  // const template = await ledger.getTemplateByName("LifeCycleEvent")
  // const contract = await ledger.getContractById("#5:1")
  // await contract.exec("LifeCycleEvent_Reply", { replyComment: {  
  //         "correctedContent":"55555",
  //         "commentText":"66666"}})
  // await contract.exec("LifeCycleEvent_Question", { questionComment: {  
  //         "correctedContent":"55555",
  //         "commentText":"66666"}})
  // const contract = await template.create({  
  //      "trade":{  
  //         "product":{  
  //            "symbol":"STOMP80187",
  //            "isin":"XS1889944580",
  //            "underlyings":[  
  //               "uuid:0001365417",
  //               "uuid:0205641225"
  //            ],
  //            "initSpots":[  
  //               1571.49,
  //               71.15
  //            ],
  //            "payoffCurrency":"iso:JPY",
  //            "startDate":"2018-11-19",
  //            "endDate":"2019-11-19",
  //            "frequency":"Monthly"
  //         },
  //         "issuer":"ISSUER-01",
  //         "buyer":"CLIENT-01",
  //         "issueDate":"2018-11-19",
  //         "notional":50000000
  //      },
  //      "comment":{  
  //         "correctedContent":"",
  //         "commentText":""
  //      },
  //      "content":"xxxx",
  //      "status":"LifeCycleEventStatusEnum_Proposed",
  //      "eventType":"LifeCycleEventTypeEnum_Cpn_Payment",
  //      "eventDate":"2019-02-04"
  //   })
}

// test()
